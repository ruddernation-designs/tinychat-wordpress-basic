<?php
/*
* Plugin Name: Wordpress Video Chat Basic - Updated On GitHub Only!
* Author: Ruddernation Designs
* Author URI: https://github.com/Ruddernation-Designs/
* Description: TinyChat full screen video chat for WordPress/BuddyPress, This also has smileys enabled using my embed file.
* Requires at least: WordPress 4.0, BuddyPress 2.0
* Tested up to: WordPress 4.5, BuddyPress 2.5
* Version: 1.0.9
* License: GPLv3
* License URI: http://www.gnu.org/licenses/gpl-3.0.html
* Date: 15th April 2016
*/
register_activation_hook(__FILE__, 'rnd_social_chat_install');
function rnd_social_chat_install() {
	global $wpdb, $wp_version;
	$post_date = date("Y-m-d H:i:s");
	$post_date_gmt = gmdate("Y-m-d H:i:s");
	$sql = "SELECT * FROM ".$wpdb->posts." WHERE post_content LIKE '%[rnd_social_page]%' AND `post_type` NOT IN('revision') LIMIT 1";
	$page = $wpdb->get_row($sql, ARRAY_A);
	if($page == NULL) {
		$sql ="INSERT INTO ".$wpdb->posts."(

			post_author, post_date, post_date_gmt, post_content, post_content_filtered, post_title, post_excerpt,  post_status, comment_status, ping_status, post_password, post_name, to_ping, pinged, post_modified, post_modified_gmt, post_parent, menu_order, post_type)

			VALUES

			('1', '$post_date', '$post_date_gmt', '[rnd_social_page]', '', 'rnd_social_chat', '', 'publish', 'closed', 'closed', '', 'rn_social_chat', '', '', '$post_date', '$post_date_gmt', '0', '0', 'page')";

		$wpdb->query($sql);
		$post_id = $wpdb->insert_id;
		$wpdb->query("UPDATE $wpdb->posts SET guid = '" . get_permalink($post_id) . "' WHERE ID = '$post_id'");
	} else {
		$post_id = $page['ID'];
	}
	update_option('rnd_social_chat_url', get_permalink($post_id));
}
add_filter('the_content', 'wp_show_rnd_social_chat_page', 56);

function wp_show_rnd_social_chat_page($content = '') {

	if(preg_match("/\[rnd_social_page\]/",$content)) {

		wp_show_rnd_social_chat();

		return "";
	}
	return $content;
}
function wp_show_rnd_social_chat() {
	$current_user = wp_get_current_user();
	$room = 'room_name';  //Replace the tinychat room name to your room where 'room_name' is, example 'ruddernation'
	$prohash = hash('sha256',filter_input(INPUT_SERVER, 'HTTP_CLIENT_IP'));
	$current_user = wp_get_current_user();
{
		echo '<style>#chat{position:fixed;left:0px;right:0px;bottom:0px;height:98%;width:100%;z-index:9999}</style><div id="chat">
<script type="text/javascript">
var tinychat = ({
		room: "'.$room.'", 
		prohash: "'.$prohash.'",
		nick: "'.$current_user->display_name.'",
		wmode:"transparent",
		chatSmileys:"true", 
		youtube:"all",
		urlsuper: "'.filter_input(INPUT_SERVER, 'HTTP_HOST'). filter_input(INPUT_SERVER, 'REQUEST_URI').'", 
		desktop:"true",
		langdefault:"en"});
		</script>
<script src="https://www.ruddernation.com/info/js/eslag.js"></script>
<div id="client"></div></div>';}}?>
