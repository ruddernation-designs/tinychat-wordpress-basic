=== Wordpress Video Chat Basic ===

Contributors: ruddernation
Donate link: https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=FFAC7FBEBH6JE
Tags: video chat, tinychat, chat, wordpress chat, buddypress chat, wordpress video chat, buddypress video chat
Requires at least: 4.0
Tested up to: 4.5
Stable tag: 1.0.9
License: GPLv3
License URI: http://www.gnu.org/licenses/gpl-3.0.html

== Description ==

TinyChat full screen video chat for WordPress and BuddyPress, You customise the 'room_name' and add your own tinychat room for you website visitors to join your room, Just change the 'room_name' in the plugins folder to your Tinychat room and save.

== Installation ==

This will automatically create the page and install the short code with link *domain name/rnd_social_chat*, If it does not then please read the rest below.

Simply use shortcode [rnd_social_chat] in a page, you may need to remove footer as some will show in front of the chat screen.
This also uses my modified Tinychat embed file, This get's updated externally so you'll never need to update it.
If you want to use Tinychats original then you'll have to change the embed file url to http://tinychat.com/embed/chat.js.
Some themes may have issues with keyboard shortcuts, Especially for those that use jetpack, You'll have to disable the admin navbar from your frontend by adding 
// Remove Admin Bar Front End
add_filter('show_admin_bar', '__return_false');
to your themes functions.php file.

== Screenshots ==

* TinyChat load screen, Users of TinyChat will notice the colour change.
* Once loaded you'll have to enter a name.
* This is showing the YouTube/Soundcloud buttons.
* Both show popups which you just enter what you'd like to search.
* This is how it displays with both YouTube and Soundcloud.
* Click this to maximize the video size, This also works when you're on camera.
* This is how it looks using the larger video's.

== Notes ==

This is full screen video chat that is used on TinyChat, This will use your domain name as the chatroom name,
This way it'll never conflict with other rooms on TinyChat,
If anyone else requires another feature or has an idea for me to implement, Then open a support ticket with the relevant information.

== Frequently Asked Questions ==

* Q. Can I use this if I'm not logged in?
* A. Yes!, But you can disable guest access via the code in tinychat.php file.

* Q. The chat is not loading for me.
* A. Check to see if you have the Adobe flash player installed (http://helpx.adobe.com/flash-player.html) and JavaScript enabled in your browser.

* Q. How do I add it to my blog/website?
* A. Just go to the backend and on appearance select menus, From there you can add your page, It'll be *chatroom* by default.

* Q. I'm having issues with my wordpress keyboard shortcuts affecting my chat, It's not allowing me to use certain letters.
* A. The fix for this is to disable the Admin navbar on your frontend only, to do this add * // Remove Admin Bar Front End
add_filter('show_admin_bar', '__return_false'); * to you funtions.php file in your themes folder.

* Q. My camera/Audio is not working using my Macbook with iSight and I cannot click "Allow"
* A. Ok, Go to http://www.macromedia.com/support/documentation/en/flashplayer/help/settings_manager06.html and select always allow for tinychat.com, Then re-try your cam/mic.

== Changelog ==

= 1.0.1 =
* First live version.
* Enter your room name and you're good to good for you WordPress guests/users.

= 1.0.4 =
* Major update, Please update to latest version for chat to work, 
This now shows the Google reCaptcha that TinyChat use, You may need to click on the shield to display unsecured content as TinyChat don't currently have the encryption set.

= 1.0.8 =
* Major update, Chat is now layed out for much better reading and editing, Please remember to change room_name to your desired Tinychat chat room, This has YouTube videos enabled for all users, you can disable this by removing 
youtube:"all" from the source, Smileys are also enabled, if you want to disable them set the value to "false".

== Social Sites ==

* Website - https://www.ruddernation.com

* Room Spy - https://www.tinychat-spy.com

* Facebook - https://www.facebook.com/rndtc

* Github - https://github.com/ruddernation

* GitHub Repositories - https://ruddernation-designs.github.io

* Google Plus - https://plus.google.com/+Ruddernation/posts

* Google Community - https://plus.google.com/communities/100073731487570686181

* Twitter - https://twitter.com/_ruddernation_
